package fp.daw.exprog20210902;

import java.util.ArrayList;
import java.util.Arrays;

public class PruebaAutomovil {

	public static void main(String[] args) {
		
		
		Automovil auto1=new Automovil("opel","capri","H6745",2000,Tipo.HIBRIDO,150);
		Automovil auto2=new Automovil("seat","berlina","F2014",1900,Tipo.DIESEL,70);
		Automovil auto3=new Automovil("renault","ranchera","P4678",2001,Tipo.GASOLINA,110);
		Automovil auto4=new Automovil("opel","capri","C4783",2003,Tipo.DIESEL,100);
		
		Automovil [] automovil=new Automovil[4];
		
        automovil[0]=auto1;
        automovil[1]=auto2;
        automovil[2]=auto3;
        automovil[3]=auto4;
        
        System.out.println("SIN ORDENAR");
        
        for (Automovil a:automovil) {
        	System.out.println(a.toString());
        }
        
        ComparaMotorPotencia comparaAuto=new ComparaMotorPotencia();
        
        Arrays.sort(automovil, comparaAuto);;
        
        System.out.println("ORDENADO");
        
        for (Automovil a:automovil) {
        	System.out.println(a.toString());
        }

	}

}
	
