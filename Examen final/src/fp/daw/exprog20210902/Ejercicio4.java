package fp.daw.exprog20210902;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.LinkedList;
import java.util.Queue;

public class Ejercicio4 {

	public static void main(String[] args) {
		
		Queue <Integer> cola=new LinkedList<>();
		cola.offer(1);
		cola.offer(8);
		cola.offer(7);
		cola.offer(2);
		cola.offer(9);
		cola.offer(18);
		cola.offer(12);
		cola.offer(0);
		System.out.println(cola);
		
		invertirImpares(cola);

	}

	private static void invertirImpares(Queue<Integer> cola) {
		Deque <Integer> pila=new ArrayDeque<>();
		Queue <Integer> cola1=new LinkedList<>();
		int i=2;
		while (!cola.isEmpty()) {
			if (i%2!=0) {
			
				pila.push(cola.poll());
			} else cola1.offer(cola.poll());
			i++;
			
		}
		System.out.println("pila "+pila);
		System.out.println("cola "+cola1);
		System.out.println("cola inicial "+ cola);
		int j=2;
		int num=pila.size()+cola1.size()+2;
		while (j<num) {
			if (j%2!=0) {			
				cola.offer(pila.pop());
			} else cola.offer(cola1.poll());
			j++;
			
		}
		System.out.println(cola);
		
	}

}
