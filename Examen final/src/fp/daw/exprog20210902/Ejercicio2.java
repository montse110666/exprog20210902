package fp.daw.exprog20210902;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

public class Ejercicio2 {

	public static void main(String[] args) {
		
		Map<String,Integer> mapa=new TreeMap<>();
		mapa.put("Ana", 22);
		mapa.put("Luis", 25);
		mapa.put("Pablo", 25);
		mapa.put("Elena", 20);
		mapa.put("Alicia", 21);
		mapa.put("C�sar", 20);
		mapa.put("Lara", 20);
		mapa.put("Tom�s", 21);
		mapa.put("Victor", 22);
		
		int edad=edadMenosFrecuente(mapa);
		System.out.println("La edad que menos se repite es: "+edad);
			
		
	}

	private static int edadMenosFrecuente(Map<String, Integer> mapa) {
		int [] array=new int[9];
		int i=0;
		for (Map.Entry<String,Integer> m:mapa.entrySet()) {
			array[i]=m.getValue();	
			i++;
		}
		
		Arrays.sort(array);
		for (int a:array) {
			System.out.println(a);
			}
		int aux=array[0];
		int cont=1;
		Map<Integer,Integer> mapa1=new TreeMap<>();
		for (int j=1;j<array.length;j++) {
			if (array[j]==aux){
				cont++;
			}else {
				mapa1.put(aux, cont);
				aux=array[j];				
				cont=1;
			}
			if (j==array.length-1) {
				mapa1.put(aux, cont);
				
			}
		}
		System.out.println(mapa1);
		int edad=recorrerMapa(mapa1,cont);
		System.out.println(cont);
		return edad;
	}

	private static int recorrerMapa(Map<Integer, Integer> mapa1, int cont) {
		
		int aux=0;
		int edad=0;
		for (Map.Entry<Integer,Integer> m:mapa1.entrySet()) {
			
			if (m.getValue()<=cont && edad<=m.getKey()) {
				    
					aux=m.getValue();
					edad=m.getKey();
					cont=aux;
			}
			
		}
		return edad;
	}

}
