package fp.daw.exprog20210902;

import java.util.Comparator;

public class ComparaMotorPotencia implements Comparator<Automovil>{

	@Override
	public int compare(Automovil arg0, Automovil arg1) {
		
		if (arg0.getMotor().compareTo(arg1.getMotor())==0) {
			return arg0.getPotencia().compareTo(arg1.getPotencia());
		}else return arg0.getMotor().compareTo(arg1.getMotor());
		
	}

}
